<?php

namespace App\Http\Controllers;

class MathematicsController extends Controller
{

    public function getClassification($integer)
    {
        //check if number and integer
        if(is_numeric($integer) && ctype_digit((string)$integer)){

            $aliquot_sum = 0;
            $input_number = (int)$integer;

            //check for divisors
            for($i = 0; $i < $input_number; $i++)
            {
                //if divisor found add it to the sum
                if(($i !== 0) && ($input_number % $i == 0))
                {
                    $aliquot_sum += $i;

                    //the number is abundant
                    if($aliquot_sum > $input_number)
                    {
                        return response()->json([
                            'status'    => 'Success',
                            'message'   => 'Abundant',
                        ]);
                    }
                }
            }

            //the number is perfect
            if($aliquot_sum === $input_number )
            {

                return response()->json([
                    'status'    => 'Success',
                    'message'   => 'Perfect'
                ]);
            }
            elseif($aliquot_sum < $input_number ) // just to make sure, even it's the only option that remained
            {

                //the number is deficient
                return response()->json([
                    'status'    => 'Success',
                    'message'   => 'Deficient',
                ]);
            }

        }

        return response()->json([
            'status'    => 'Error',
            'message'   => 'Invalid input'
        ]);

    }
}